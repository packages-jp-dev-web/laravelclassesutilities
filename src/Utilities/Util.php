<?php

namespace JpUtilities\Utilities;

use NumberFormatter;

class Util
{
    public static function message($string)
    {
        $json = json_decode(file_get_contents(app_path().'/filesutils/message.json'));

        return $json->$string;
    }

    public static function success($string)
    {
        $json = json_decode(file_get_contents(app_path().'/filesutils/success.json'));

        return $json->$string;
    }

    public static function error($string)
    {
        $json = json_decode(file_get_contents(app_path().'/filesutils/error.json'));

        return $json->$string;
    }

    public static function toView($value)
    {
        return date('d/m/Y', strtotime($value));
    }

    public static function arrayKeyExists($key, $array)
    {
        return array_key_exists($key, $array);
    }

    public static function formatHourValidate($data, $indice)
    {
        if (isset($data[$indice])) {
            if ($data[$indice] == '') {
                $data[$indice] = null;
            } elseif (count(Util::explodeUtil(':', $data['hour'])) <= 2) {
                $data[$indice] = $data[$indice].':00';
            }
            $hourValidate = $data[$indice];
        } else {
            $hourValidate = null;
        }

        return ['data' => $data, 'hourValidate' => $hourValidate];
    }

    public static function formatIntegerValidate($data, $indices)
    {
        foreach ($indices as $indice) {
            if (isset($data[$indice])) {
                if ($data[$indice] == '') {
                    $data[$indice] = 0;
                }
            }
        }

        return $data;
    }

    public static function formatIntegerofMask($data, $indices)
    {
        foreach ($indices as $indice) {
            if (isset($data[$indice])) {
                $data[$indice] = Util::formatDecimalToMysql($data[$indice]);
            }
        }

        return $data;
    }

    public static function formatDateValidate($data, $indices)
    {
        foreach ($indices as $indice) {
            if (isset($data[$indice])) {
                if ($data[$indice] == '') {
                    $data[$indice] = null;
                }
            }
        }

        return $data;
    }

    public static function formatDecimalToMysql($value)
    {
        $aux = str_replace('.', '', $value);

        return str_replace(',', '.', $aux);
    }

    public static function formatDecimalToView($value)
    {
        return str_replace('.', ',', $value);
    }

    public static function formatDecimalPtBr($value)
    {
        $formatter = new NumberFormatter('pt_BR', NumberFormatter::CURRENCY);

        return $formatter->formatCurrency($value, 'BRL');
    }

    public static function formatDecimalPtBrNotMoney($value)
    {
        $formatter = new NumberFormatter('pt_BR', NumberFormatter::CURRENCY);
        $formatter->setTextAttribute(NumberFormatter::NEGATIVE_PREFIX, '-');
        $formatter->setTextAttribute(NumberFormatter::NEGATIVE_SUFFIX, '');

        return Util::str_replaceUtil('R$', '', $formatter->formatCurrency($value, 'BRL'));
    }

    public static function differenceDateYear($date1, $date2)
    {
        \Carbon\Carbon::setLocale('pt_BR');
        $dateCount1 = new \Carbon\Carbon($date1, 'America/Sao_Paulo');
        $dateCount2 = new \Carbon\Carbon($date2, 'America/Sao_Paulo');

        return $dateCount1->diffInYears($dateCount2);
    }

    public static function differenceDateForHumans($date1, $date2, $diff)
    {
        \Carbon\Carbon::setLocale('pt_BR');
        $dateCount1 = new \Carbon\Carbon($date1, 'America/Sao_Paulo');
        $dateCount2 = new \Carbon\Carbon($date2, 'America/Sao_Paulo');

        return $dateCount1->diffForHumans($dateCount2, true, false, $diff);
    }

    public static function partOfTheDay($hours)
    {
        $hourChain = \Carbon\Carbon::createFromFormat('H:i:s', $hours);
        if ($hourChain->hour > 5 && $hourChain->hour < 13) {
            return 1;
        } elseif ($hourChain->hour > 12 && $hourChain->hour < 18) {
            return 2;
        } elseif ($hourChain->hour > 17 && $hourChain->hour < 23) {
            return 3;
        }
    }

    public static function aditionDate($dateInitial, $termAdition, $amountAdition)
    {
        $date = new \Carbon\Carbon($dateInitial, 'America/Sao_Paulo');
        //$date = \Carbon\Carbon::createFromFormat('Y-m-d', $dateInitial);
        if ($termAdition == 'd') {
            $date->addDays($amountAdition);

            return $date->formatLocalized('%Y-%m-%d');
        } elseif ($termAdition == 'm') {
            $date->addMonths($amountAdition);

            return $date->formatLocalized('%Y-%m-%d');
        } elseif ($termAdition == 'y') {
            $date->addYears($amountAdition);

            return $date->formatLocalized('%Y-%m-%d');
        }

        return $dateInitial;
    }

    public static function subtractionDate($dateInitial, $termSubtraction, $amountSubtraction)
    {
        $date = new \Carbon\Carbon($dateInitial, 'America/Sao_Paulo');
        //$date = \Carbon\Carbon::createFromFormat('Y-m-d', $dateInitial);
        if ($termSubtraction == 'd') {
            $date->subDays($amountSubtraction);

            return $date->formatLocalized('%Y-%m-%d');
        } elseif ($termSubtraction == 'm') {
            $date->subMonths($amountSubtraction);

            return $date->formatLocalized('%Y-%m-%d');
        } elseif ($termSubtraction == 'y') {
            $date->subYears($amountSubtraction);

            return $date->formatLocalized('%Y-%m-%d');
        }

        return $dateInitial;
    }

    public static function tabelValue()
    {
        return [
            'Mais do que R$ 100 mil' => 'Mais do que R$ 100 mil',
            'R$ 50 mil - R$ 100 mil' => 'R$ 50 mil - R$ 100 mil',
            'R$ 20 mil - R$ 50 mil' => 'R$ 20 mil - R$ 50 mil',
            'Até 20 mil' => 'Até 20 mil',
        ];
    }

    public static function getMonths()
    {
        return [
            'monthPortuguese' => [
                'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
                'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro',
            ],
            'monthEnglish' => [
                'January', 'February', 'March', 'April', 'May', 'June', 'July',
                'August', 'September', 'October', 'November', 'December',
            ],
        ];
    }

    public static function getMonthsAndDays()
    {
        return [
            'monthAndDaysPortuguese' => [
                'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
                'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro', 'Domingo',
                'Segunda-Feira', 'Terça-Feira', 'Quarta-Feira', 'Quinta-Feira',
                'Sexta-Feira', 'Sábado',
            ],
            'monthAndDaysEnglish' => [
                'January', 'February', 'March', 'April', 'May', 'June', 'July',
                'August', 'September', 'October', 'November', 'December', 'Sunday',
                'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday',
            ],
        ];
    }

    public static function getDays()
    {
        return [
            'monthPortuguese' => [
                'Domingo', 'Segunda', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
                'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro',
            ],
            'monthEnglish' => [
                'Sunday ', 'Monday', 'March', 'April', 'May', 'June', 'July',
                'August', 'September', 'October', 'November', 'December',
            ],
        ];
    }

    public static function toMySQL($value)
    {
        $date = explode('/', $value);

        return $date[2].'-'.$date[1].'-'.$date[0];
    }

    public static function truncate($string)
    {
        return current(explode('\n', wordwrap($string, 70, ' ...\n')));
    }

    public static function explodeUtil($delimiter, $data)
    {
        return explode($delimiter, $data);
    }

    public static function implodeUtil($delimiter, $data)
    {
        if (is_array($data)) {
            return implode($delimiter, $data);
        }

        return '';
    }

    public static function str_replaceUtil($stringSearch, $stringSubstitute, $stringOring)
    {
        return str_replace($stringSearch, $stringSubstitute, $stringOring);
    }

    public static function getKeyGoogle()
    {
    }

    public static function getUfs()
    {
        return array('AC' => 'AC', 'AL' => 'AL', 'AP' => 'AP', 'AM' => 'AM',
            'BA' => 'BA', 'CE' => 'CE', 'DF' => 'DF', 'ES' => 'ES', 'GO' => 'GO',
            'MA' => 'MA', 'MT' => 'MT', 'MS' => 'MS', 'MG' => 'MG', 'PA' => 'PA',
            'PB' => 'PB', 'PR' => 'PR', 'PE' => 'PE', 'PI' => 'PI', 'RJ' => 'RJ',
            'RN' => 'RN', 'RS' => 'RS', 'RO' => 'RO', 'RR' => 'RR', 'SC' => 'SC',
            'SP' => 'SP', 'SE' => 'SE', 'TO' => 'TO', );
    }

    public static function getFunctionalities()
    {
        $functionalities = \Illuminate\Support\Facades\DB::table('type_functionalities')
                ->get();
        $result = [];
        //$result['Escolha alguma(s) funcionalidade(s)'] = 'Escolha alguma(s) funcionalidade(s)';
        foreach ($functionalities as $functionality) {
            $result[$functionality->name] = $functionality->name;
        }
        $result2['Escolha alguma(s) funcionalidade(s)'] = $result;

        return $result2;
    }

    public static function getPublicPlace()
    {
        return [
            'Rua' => 'Rua',
            'Avenida' => 'Avenida',
            'Alameda' => 'Alameda',
            'Bloco' => 'Bloco',
            'Fazenda' => 'Fazenda',
            'Praça' => 'Praça',
            'Travessa' => 'Travessa',
            'Quadra' => 'Quadra',
            'Vila' => 'Vila',
        ];
    }

    //Urls

    /**
     * Gera uma qr code com a url de redirecionamento.
     *
     * @param string $url
     *
     * @return string
     */
    public static function generateUrlQrCode($url)
    {
        return 'https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl='.$url;
    }

    public static function getUrlShareFace($url, $urlRedirection)
    {
        $appId = '223014254906845';
        $urlBase = 'https://www.facebook.com/dialog/share?app_id='.$appId.'&display=popup';
        $urlShare = '&href='.str_replace([':', '/'], ['%3A', '%2F'], $url);
        $urlRedirectionAfterRedirection = '&redirect_uri='.str_replace([':', '/'], ['%3A', '%2F'], $urlRedirection);

        return $urlBase.$urlShare.$urlRedirectionAfterRedirection;
    }

    public static function getUrlShareTwitter($url, $name, $description)
    {
        $urlBase = 'https://twitter.com/intent/tweet?original_referer=';
        $urlShare = str_replace([':', '/'], ['%3A', '%2F'], $url);
        $nameFormated = '&text='.str_replace([' ', ',', '-'], ['%20', '%52', '%65'], $name);
        $descriptionFormated = str_replace([' ', ',', '-'], ['%20', '%52', '%65'], $description);

        return $urlBase.$urlShare.'&ref_src=twsrc%5Etfw'.$nameFormated.$descriptionFormated.'&tw_p=tweetbutton&url='.$urlShare;
    }

    public static function getUrlShareGoogle($url)
    {
        return 'https://plus.google.com/share?app=110&url='.$url;
    }

    //News Functions

    /**
     * Generate Password Random.
     *
     * @param string $size      Size of Password (default = 6)
     * @param bool   $uppercase If true use letter uppercase (default = true)
     * @param bool   $lowercase If true use letter lowercase (default = true)
     * @param bool   $number    If true use numbers     (default = true)
     * @param bool   $symbol    If true use symbols (default = true)
     *
     * @return string
     */
    public static function generatePassword($size = 6, $uppercase = true, $lowercase = true, $number = true, $symbol = true)
    {
        $stringLetterUppercase = 'ABCDEFGHIJKLMNOPQRSTUVYXWZ';
        $stringLetterLowercase = 'abcdefghijklmnopqrstuvyxwz';
        $stringNumber = '0123456789';
        $stringSymbol = '!@#$%¨&*()_+=';
        $password = '';
        if ($uppercase) {
            $password .= str_shuffle($stringLetterUppercase);
        }

        if ($lowercase) {
            $password .= str_shuffle($stringLetterLowercase);
        }

        if ($number) {
            $password .= str_shuffle($stringNumber);
        }

        if ($symbol) {
            $password .= str_shuffle($stringSymbol);
        }

        return substr(str_shuffle($password), 0, $size);
    }
}
