<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JpUtilities\Utilities;
/**
 * Description of EncryptUtility
 *
 * @author Jefferson
 */
class EncryptUtility {

    /**
     * Encrypt string sample
     * @param string $value String for are encrypt
     * @return string
     */
    public static function encryptSample($value) {
        $salt = EncryptUtility::getSalt();
        return utf8_encode(openssl_encrypt($value, 'aes-128-cbc', $salt, true, $salt));
    }

    /**
     * Decrypt string sample
     * @param string $crypt String encrypt for are decrypt
     * @return string
     */
    public static function decryptSample($crypt) {
        $salt = EncryptUtility::getSalt();
        return openssl_decrypt(utf8_decode($crypt), 'aes-128-cbc', $salt, true, $salt);
    }

    public static function numHash($number){
        return (((0x0000FFFF & $number) << 16) + ((0xFFFF0000 & $number) >> 16)); 
    }

    private static function getSalt() {
        return substr(env('APP_KEY'), -16);
    }

}
