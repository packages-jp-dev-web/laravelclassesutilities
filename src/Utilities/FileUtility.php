<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JpUtilities\Utilities;

//Exception
use Symfony\Component\HttpFoundation\File\Exception\FileException;
//Default
use Illuminate\Support\Facades\File;

//Utilities
use JpUtilities\Utilities\LogsSystem;
/**
 * Description of FileUtility.
 *
 * @author Jefferson
 */
class FileUtility
{
    /*
     * Exclui o file do servidor de arquivos, em caso de sucesso retorna true
     * caso contrário retorna false
     *
     * @param $string $way Caminho para o arquivo
     * @return  true/false
     */

    public static function deleteFile($way)
    {
        try {
            if (File::delete($way)) {
                return true;
            }

            return false;
        } catch (FileException $ex) {
            $logs = new LogsSystem();
            $logs->writeLog($ex->getMessage().'/FileUtility - deleteImage');

            return false;
        }
    }

    /*
     * Exclui o file do servidor de arquivos diretamente do diretório público,
     * em caso de sucesso retorna true caso contrário retorna false
     *
     * @param $string $way Caminho para o arquivo
     * @return  true/false
     */

    public static function deleteFileDirectoryPublic($way)
    {
        try {
            if (File::delete(public_path().'/'.$way)) {
                return true;
            }

            return false;
        } catch (FileException $ex) {
            $logs = new LogsSystem();
            $logs->writeLog($ex->getMessage().'/FileUtility - deleteImage');

            return false;
        }
    }
}
