<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JpUtilities\Utilities;

/**
 * Description of LogsSystem
 *
 * @author Jefferson
 */
class LogsSystem {

    private function getFile() {
        return 'logsystem.txt';
    }

    private function getFileEmail() {
        return 'logsystememail.txt';
    }

    private function getFileJob() {
        return 'logsystemjob.txt';
    }

    public function writeLog($data) {
        $file = fopen($this->getFile(), 'a');
        fwrite($file, date('d-m-y H:m') . ' | ' . $data . PHP_EOL);
        fclose($file);
    }

    public function writeLogEmail($data) {
        $file = fopen($this->getFileEmail(), 'a');
        fwrite($file, date('d-m-y H:m') . ' | ' . $data . PHP_EOL);
        fclose($file);
    }
     public function writeLogJob($data) {
        $file = fopen($this->getFileJob(), 'a');
        fwrite($file, date('d-m-y H:m') . ' | ' . $data . PHP_EOL);
        fclose($file);
    }

}
