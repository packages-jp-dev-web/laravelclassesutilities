<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JpUtilities\Utilities;

use Carbon\Carbon;

/**
 * Description of DateUtility.
 *
 * @author Jefferson
 */
class DateUtility
{
    public static function setLocalePt()
    {
        setlocale(LC_ALL, 'pt_BR');
    }

    public static function dateForHumans($date)
    {
        $dateCarbon = new Carbon($date, 'America/Sao_Paulo');
        DateUtility::setLocalePt();

        return $dateCarbon->formatLocalized('%d de %B %Y'); //->formatLocalized('%a ');//->toDateTimeString('MMMM Do YYYY, h:mm:ss a'); // //->isoFormat('MMMM Do YYYY, h:mm:ss a');
    }

    /**
     * Return date string first Year.
     *
     * @param string $date Date for formating
     *
     * @return string
     */
    public static function dateForHumansFirstYear($date)
    {
        $dateCarbon = new Carbon($date, 'America/Sao_Paulo');
        DateUtility::setLocalePt();

        return $dateCarbon->formatLocalized('%d de %B %Y'); //->formatLocalized('%a ');//->toDateTimeString('MMMM Do YYYY, h:mm:ss a'); // //->isoFormat('MMMM Do YYYY, h:mm:ss a');
    }
}
