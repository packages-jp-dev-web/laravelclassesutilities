<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JpUtilities\Utilities;

use Symfony\Component\HttpFoundation\File\UploadedFile;
//Exception
use Symfony\Component\HttpFoundation\File\Exception\FileException;
//Utilities
use JpUtilities\Utilities\LogsSystem;
use JpUtilities\Utilities\StringUtility;

/**
 * Description of Upload.
 * Classe facility upload files.
 *
 * @author Jefferson
 */
class Upload
{
    /**
     * Make upload of files.
     *
     * @param UploadedFile $file File for upload
     * @param string $name Name of file
     * @param string $way Way for save file
     *
     * @return string/null
     */
    public static function upload($file, $name, $way)
    {
        try {
            if (!$file->isValid()) {
                return null;
            }
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $fileName =  StringUtility::generateSlugOfText($name) . '.' . $extension; // renameing image
            if ($file->move(\public_path() . '/' . $way, $fileName)) { // uploading file to given path
                return $way . '/' . $fileName;
            }
            return null;
        } catch (FileException $ex) {
            $logs = new LogsSystem();
            $logs->writeLog($ex->getMessage() . 'App\UtilitiesJp\Upload - upload');
            return null;
        }
    }
}