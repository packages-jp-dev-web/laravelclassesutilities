<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JpUtilities\Utilities;

use Illuminate\Support\Str;
/**
 * Description of StringUtility.
 *
 * @author Jefferson
 */
class StringUtility
{
    /**
     * Generate text without special characters, with increment time().
     *
     * @param [string] $text
     *
     * @return string
     */
    public static function generateSlugOfText($text)
    {
        return  Str::slug($text.time());
    }

    /**
     * Generate text without special characters.
     *
     * @param [string] $text
     *
     * @return string
     */
    public static function generateSlugOfTextWithComplement($text)
    {
        return  Str::slug($text);
    }
}
