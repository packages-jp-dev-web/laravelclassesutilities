<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JpUtilities\Utilities;

/**
 * Description of ArrayUltility.
 *
 * @author Jefferson
 */
class ArrayUtility
{
    public static function convertArrayForInputSelect($tag_chave, $tag_value, $vetor)
    {
        try {
            $result = [];
            foreach ($vetor as $element) {
                $result[$element[$tag_chave]] = $element[$tag_value];
            }

            return $result;
        } catch (Exception $ex) {
            return [];
        }
    }

    public static function convertArrayForInputSelectWith2Value($tag_chave, $tag_value, $tag_value2, $vetor)
    {
        try {
            $result = [];
            foreach ($vetor as $element) {
                if ($element[$tag_value2] != '') {
                    $result[$element[$tag_chave]] = $element[$tag_value].' - '.$element[$tag_value2];
                } else {
                    $result[$element[$tag_chave]] = $element[$tag_value];
                }
            }

            return $result;
        } catch (Exception $ex) {
            return [];
        }
    }

    public static function convertArrayForInputSelectWith2ValueOptinal($tag_chave, $tag_value, $tag_value2, $vetor)
    {
        try {
            $result = [];
            foreach ($vetor as $element) {
                if ($element[$tag_value] != '') {
                    $result[$element[$tag_chave]] = $element[$tag_value];
                } else {
                    $result[$element[$tag_chave]] = $element[$tag_value2];
                }
            }

            return $result;
        } catch (Exception $ex) {
            return [];
        }
    }
}
