<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JpUtilities\Utilities;

/**
 * Description of MountOpenGraph.
 *
 * @author Jefferson
 */
class MountOpenGraph
{
    public static function getFbAppId($appId)
    {
        return '<meta property="fb:app_id"             content="'.$appId.'"/>';
    }

    public static function getTitle($title)
    {
        return '<meta property="og:title" content="'.$title.'">';
    }

    public static function getDescription($description)
    {
        return '<meta property="og:description" content="'.$description.'">';
    }

    public static function getImage($image)
    {
        return '<meta property="og:image" content="'.$image.'"/>';
    }

    public static function getUrl($url)
    {
        return '<meta property="og:url" content="'.$url.'">';
    }

    public static function getType($type)
    {
        return '<meta property="og:type" content="'.$type.'" />';
    }

    public static function getIdButtonSharFacebook()
    {
        return 'shareBtn';
    }

    public static function getCodeFacebook($appId, $url)
    {
        return "<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '".$appId."',
      xfbml      : true,
      version    : 'v3.0'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = 'https://connect.facebook.net/en_US/sdk.js';
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   document.getElementById('shareBtn').onclick = function() {
                        FB.ui({
                         
                            method: 'share',
                            mobile_iframe: true,
                            href: '".$url."',
                        }, function(response){});
                    }
</script>";
    }
}
