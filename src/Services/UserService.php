<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JpUtilities\Services;

//Utilities
use JpUtilities\Utilities\LogsSystem;
use JpUtilities\Utilities\Upload;
use JpUtilities\Utilities\FileUtility;
use JpUtilities\Utilities\StringUtility;
use JpUtilities\Utilities\Util;
//Services
use ServiceDefault;
//Model
use App\User;
//Default
use Illuminate\Support\Facades\Hash;

/**
 * Description of UserService.
 *
 * @author Jefferson
 */
class UserService extends LogsSystem implements ServiceDefault
{
    //put your code here
    public function create($data)
    {
        $resultUpload = null;
        $way = $this->getImageUserDefault();
        $data['slug'] = StringUtility::generateSlugOfTextWithComplement('usuario-'.rand(1, 9).time());
        if (isset($data['image'])) {
            $resultUpload = Upload::upload($data['image'], $data['slug'], 'images/users');
        }
        if ($resultUpload != null) {
            $way = $resultUpload;
        }
        $data['avatar'] = $way;
        $data['password'] = Hash::make($data['password']);

        return User::create($data);
    }

    public function deleteWithId($id)
    {
    }

    public function deleteWithSlug($slug)
    {
    }

    public function edit($data)
    {
        $user = User::findOrFail($data['id']);
        $way = null;
        if (isset($data['image'])) {
            $way = Upload::upload($data['image'], $user->slug, 'images/users');
        }
        if ($way) {
            $wayimage = $user->avatar;
            if ($wayimage != $this->getImageUserDefault()) {
                FileUtility::deleteFileDirectoryPublic($wayimage);
            }
            $data['avatar'] = $way;
        }
        if (isset($data['password'])) {
            if ($data['password'] != '' && $data['password'] != null) {
                $data['password'] = Hash::make($data['password']);
            } else {
                unset($data['password']);
            }
        } else {
            if (array_key_exists('password', $data)) {
                unset($data['password']);
            }
        }
        if ($user->update($data)) {
            return $user;
        }

        return null;
    }

    public function getAll()
    {
    }

    public function getAllForSelect()
    {
    }

    public function getMessages()
    {
        return [
            'required' => 'Preencha este campo.',
            'max' => 'O campo deve ter no máximo :max caracteres.',
            'confirmed' => 'As senhas devem ser iguais.',
            'image.mimes' => 'Os tipos de arquivos aceitos são .jpeg, .bmp, .png .',
            'image.max' => 'O tamanho máximo do arquivo 300kb.',
            'image.required' => 'Escolha uma imagem',
            'password.required' => 'Entre com uma senha',
            'email.required' => 'Entre com um email.',
            'email.unique' => 'E-mail já cadastrado em nossa base dados.',
            'email' => 'Formato de email inválido.',
        ];
    }

    public function getRules($type, $parameters)
    {
        $result = [];
        switch ($type) {
            case 'edit':
                $result = [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users,email,'.$parameters['id'],
                    'password' => 'confirmed|max:255',
                    'image' => 'max:300|mimes:jpeg,bmp,png,jpg',
                ];
                break;
        }

        return $result;
    }

    public static function getMessageReturn($slug)
    {
        $messages = '{
                "SolicitationError":"Desculpe algo deu errado, tente novamente mais tarde.",
                "EmailNull":"Email não encontrado em nossa base de dados.",
                "ResetSuccess":"Sua senha foi resgatada, verifique em seu e-mail a nova senha."
                }';
        $json = json_decode($messages);

        return $json->$slug;
    }

    public function getWithId($id)
    {
    }

    public function getWithSlug($slug)
    {
    }

    /**
     * Get user per email.
     *
     * @param string $email Email of User
     *
     * @return User|null
     */
    public function getPerEmail($email)
    {
        return User::where('email', $email)->first();
    }

    public function getImageUserDefault()
    {
        return '/images/users/avatar.jpg';
    }

    /**
     * Reset password User.
     *
     * @param string $email Email of User
     *
     * @return string
     */
    public function resetPassword($email)
    {
        $user = User::where('email', $email)->first();
        if (!$user) {
            return null;
        }
        $password = Util::generatePassword();
        $user->password = Hash::make($password);
        $user->save();

        return $password;
    }
}
