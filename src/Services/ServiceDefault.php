<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JpUtilities\Services;

/**
 *
 * @author Jefferson
 */
interface ServiceDefault
{
    /**
     * Create Entity
     * @param array $data Data of entity
     */
    public function create($data);

    public function edit($data);

    public function getWithId($id);

    public function getWithSlug($slug);

    public function getAll();
   
    public function getAllForSelect();

    public function deleteWithId($id);

    public function deleteWithSlug($slug);
   
    public function getRules($type, $parameters);
   
    public function getMessages();
}
