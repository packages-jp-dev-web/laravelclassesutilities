<?php

namespace JpUtilities\Services;

use Illuminate\Support\Facades\Mail;
use JpUtilities\Utilities\LogsSystem;
/**
 * Description of ContactService.
 *
 * Classe para envio de emails
 * Autor: Jefferso Pereira
 * Data: 18/02/2018
 *
 * @author Jefferson
 */
class ContactService
{
    /*
     * Method for send email recipient
     * $data = Data the email
     * $emailRecipient= email the recipient
     */

    public static function sendEmail($emailRecipient, $templateEmail)
    {
        try {
            Mail::to($emailRecipient)
                    ->send($templateEmail);
            if (count(Mail::failures()) < 1) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $ex) {
            $logs = new LogsSystem();
            $logs->writeLogEmail($ex->getMessage().$ex->getFile().$ex->getLine().'/ ContactService - sendEmail');

            return false;
        }
    }

    public static function sendEmailWithCc($emailRecipient, $emailCc, $templateEmail)
    {
        try {
            if ($emailCc != null && $emailCc != '') {
                Mail::to($emailRecipient)
                        ->cc($emailCc)
                        ->send($templateEmail);
            } else {
                Mail::to($emailRecipient)
                        ->send($templateEmail);
            }
            if (count(Mail::failures()) < 1) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $ex) {
            $logs = new LogsSystem();
            $logs->writeLogEmail($ex->getMessage().$ex->getFile().$ex->getLine().'/ ContactService - sendEmailWithCc');

            return false;
        }
    }

    public static function getRules($type)
    {
        $result = [];
        switch ($type) {
            case 'email-required':
                $result = [
                    'email' => 'sometimes|required|email',
                    'name' => 'sometimes|required',
                    'message' => 'sometimes|required',
                ];
                break;
        }

        return $result;
    }

    public static function getMessages()
    {
        return [
          'email.required' => 'Forneça um e-mail para que possamos entrar em contato.',
            'email.email' => 'Formato de email inválido.',
            'name.required' => 'Como você se chama?',
            'message.required' => 'Descreva nos o motivo de seu contato.',
        ];
    }

    public static function getMessageReturn($slug)
    {
        $messages = '{
                "SolicitationError":"Desculpe algo deu errado, tente novamente mais tarde.",
                "SendSuccessDefault":"Em breve retornaremos seu contato."
                }';
        $json = json_decode($messages);

        return $json->$slug;
    }
}
